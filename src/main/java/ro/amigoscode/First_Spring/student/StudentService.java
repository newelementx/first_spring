package ro.amigoscode.First_Spring.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service public class StudentService {

	private final StudentRepository studentRepository;

	//    @Autowired    // merge si fara, pentru ca StudentRepository foloseste adnotarea @Repository care are acelasi efect ca @Component
	public StudentService(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	public List<Student> getStudents() {
		return studentRepository.findAll();
	}

	public void addNewStudent(Student student) {
		Optional<Student> studentOptional = studentRepository.findStudentByEmail(student.getEmail());
		if (studentOptional.isPresent()) {
			throw new IllegalStateException("email taken");
		}

		studentRepository.save(student);
	}

	public void deleteStudent(Long studentId) {
		boolean exists = studentRepository.existsById(studentId);
		if (!exists) {
			throw new IllegalStateException("Student with id: " + studentId + " does not exists");
		}
		studentRepository.deleteById(studentId);
	}

//	@Transactional(rollbackOn = {})  // I don't know why I put this here
	public void updateStudent(Long studentId, String name, String email) {
		Student student = studentRepository.findById(studentId)
				.orElseThrow(() -> new IllegalStateException("Student with id: " + studentId + " does not exists"));

		if (name != null && name.length() > 0 && !Objects.equals(student.getName(), name)) {
			// Here we check that the new name is not the same as the old one
			student.setName(name);
		}

		if (email != null && email.length() > 0 && !Objects.equals(student.getEmail(), email)) {
			// Here we check that the new email is not the same as the old one
			// and then, check if the email is not used by other student.
			Optional<Student> studentOptional = studentRepository.findStudentByEmail(email);
			if (studentOptional.isPresent()) {
				throw new IllegalStateException("email taken by another student");
			}
			// and then set the new email address
			student.setEmail(email);
		}
		studentRepository.saveAndFlush(student);
	}
}
