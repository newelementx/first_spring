package ro.amigoscode.First_Spring.student;

import org.hibernate.type.TimestampType;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student maria = new Student(
                    "Maria",
                    "maria@huawei.com",
                    LocalDate.of(1996, Month.FEBRUARY, 19));

            Student alex = new Student(
                    "Alex",
                    "alexandru808@gmail.com",
                    LocalDate.of(2000, Month.JULY, 8));

            Student vlados = new Student(
                    "Vlados",
                    "newelementx808@gmail.com",
                    LocalDate.of(1996, Month.APRIL, 17));

            repository.saveAll(List.of(maria, alex, vlados));
        };
    }

}
